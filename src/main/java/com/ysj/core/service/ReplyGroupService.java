package com.ysj.core.service;

import com.ysj.core.po.ReplyGroup;

import java.util.List;

public interface ReplyGroupService {
	int createReply(ReplyGroup replyGroup);
	List<ReplyGroup> replyGrouplist(String creator);
	int deleteReply(String replyId);
}
